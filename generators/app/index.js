'use strict';

const Generator = require('yeoman-generator');
const chalk = require('chalk');
const yosay = require('yosay');

const { PostgresqlAdapter } = require('./adapters');
const { TYPES } = require('./constants');

class DbDiagramGenerator extends Generator {
  static get POSTGRESQL() {
    return 'postgresql';
  }

  static get ADAPTERS() {
    return {
      [DbDiagramGenerator.POSTGRESQL]: PostgresqlAdapter,
    };
  }

  static get DEFAULTS() {
    return {
      [DbDiagramGenerator.POSTGRESQL]: {
        username: 'postgres',
        password: 'postgresqlpassword',
        host: '127.0.0.1',
        port: '5432',
        database: 'database',
        destination: 'schema.dbdiagram',
      },
    };
  }

  async prompting() {
    this.log(
      yosay(
        `Welcome to the classy ${chalk.red('generator-dbdiagram')} generator!`
      )
    );

    const { type } = await this.prompt([{
      type: 'list',
      message: 'What type of database should I dbdiagram ?',
      name: 'type',
      choices: [DbDiagramGenerator.POSTGRESQL],
      default: DbDiagramGenerator.POSTGRESQL,
    }]);

    const prompts = [
      {
        type: 'input',
        message: 'username ?',
        name: 'username',
        default: DbDiagramGenerator.DEFAULTS[type].username,
      },
      {
        type: 'input',
        message: 'password ?',
        name: 'password',
        default: DbDiagramGenerator.DEFAULTS[type].password,
      },
      {
        type: 'input',
        message: 'host ?',
        name: 'host',
        default: DbDiagramGenerator.DEFAULTS[type].host,
      },
      {
        type: 'input',
        message: 'port ?',
        name: 'port',
        default: DbDiagramGenerator.DEFAULTS[type].port,
      },
      {
        type: 'input',
        message: 'database ?',
        name: 'database',
        default: DbDiagramGenerator.DEFAULTS[type].database,
      },
      {
        type: 'input',
        message: 'destination ?',
        name: 'destination',
        default: DbDiagramGenerator.DEFAULTS[type].destination,
      },
    ];

    const { username, password, host, port, database, destination } = await this.prompt(prompts);

    const adapter = new DbDiagramGenerator.ADAPTERS[type]({ username, password, host, port, database });

    const tables = await adapter.getProps();

    this.props = {
      tables: tables.map((table) => ({
        ...table,
        columns: table.columns.map((column) => {
          const options = [];

          if (column.primaryKey === true) {
            options.push('pk');
            options.push('unique');
            if (column.type.name === TYPES.INTEGER()) {
              options.push('increment');
            }
          }
          if (column.allowNull === false) {
            options.push('not null');
          }
          if (column.foreignKey) {
            options.push(`ref: > ${column.foreignKey.foreignTableName}.${column.foreignKey.foreignColumnName}`)
          }

          return {
            name: column.columnName,
            type: column.type,
            options: options.length ? ` [${options.join(', ')}]` : '',
          };
        })
      })),
      destination,
    };

    return;
  }

  writing() {
    this.log(this.props.tables[1]);
    this.fs.copyTpl(
      this.templatePath('example.dbdiagram'),
      this.destinationPath(this.props.destination),
      this.props
    );
  }
}

module.exports = DbDiagramGenerator;
