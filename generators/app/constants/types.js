module.exports = {
  INTEGER: () => 'integer',
  VARCHAR: (length) => `varchar(${length})`,
  JSONB: () => 'jsonb',
  TEXT: () => 'text',
  JSON: () => 'json',
  TIMESTAMP: () => 'timestamp',
  BOOLEAN: () => 'boolean',
  FLOAT: () => 'float',
  ENUM: () => 'enum',
};
