const Sequelize = require('sequelize');
const S = require('string');
const { TYPES } = require('../constants');

class PostgresqlAdapter {
  static get TYPE_INTEGER() {
    return 'INTEGER';
  }
  static get TYPE_CHARACTER_VARYING() {
    return 'CHARACTER VARYING';
  }
  static get TYPE_JSON() {
    return 'JSON';
  }
  static get TYPE_JSONB() {
    return 'JSONB';
  }
  static get TYPE_TIMESTAMP_WITH_TIME_ZONE() {
    return 'TIMESTAMP WITH TIME ZONE';
  }
  static get TYPE_TEXT() {
    return 'TEXT';
  }
  static get TYPE_BOOLEAN() {
    return 'BOOLEAN';
  }
  static get TYPE_DOUBLE_PRECISION() {
    return 'DOUBLE PRECISION';
  }
  static get TYPE_USER_DEFINED() {
    return 'USER-DEFINED';
  }
  static get TYPES_MAPPING() {
    return {
      [PostgresqlAdapter.TYPE_INTEGER]: () => TYPES.INTEGER(),
      [PostgresqlAdapter.TYPE_CHARACTER_VARYING]: (length) => TYPES.VARCHAR(length),
      [PostgresqlAdapter.TYPE_JSON]: () => TYPES.JSON(),
      [PostgresqlAdapter.TYPE_JSONB]: () => TYPES.JSONB(),
      [PostgresqlAdapter.TYPE_TIMESTAMP_WITH_TIME_ZONE]: () => TYPES.TIMESTAMP(),
      [PostgresqlAdapter.TYPE_TEXT]: () => TYPES.TEXT(),
      [PostgresqlAdapter.TYPE_BOOLEAN]: () => TYPES.BOOLEAN(),
      [PostgresqlAdapter.TYPE_DOUBLE_PRECISION]: () => TYPES.FLOAT(),
      [PostgresqlAdapter.TYPE_USER_DEFINED]: () => TYPES.ENUM(),
    }
  }

  constructor({
    username,
    password,
    host,
    port,
    database,
  }) {
    this.database = database;
    this.client = new Sequelize(
      database,
      username,
      password,
      {
        host,
        logging: false,
        dialect: 'postgres',
        port,
      }
    );
  }

  async getTableNames() {
    const rows = await this.client.query('SELECT table_name AS "tableName" FROM information_schema.tables WHERE table_schema = \'public\'', { type: Sequelize.QueryTypes.SELECT });

    return rows.map(({ tableName }) => tableName);
  }

  async getColumns(tableName) {
    const rows = await this.client.getQueryInterface().describeTable(tableName);

    return rows;
  }

  async getForeignKeys(tableName) {
    const rows = await this.client.query(`
      SELECT
        kcu.column_name AS "columnName",
        ccu.table_name AS "foreignTableName",
        ccu.column_name AS "foreignColumnName"
      FROM 
        information_schema.table_constraints AS tc 
        JOIN information_schema.key_column_usage AS kcu
          ON tc.constraint_name = kcu.constraint_name
          AND tc.table_schema = kcu.table_schema
        JOIN information_schema.constraint_column_usage AS ccu
          ON ccu.constraint_name = tc.constraint_name
          AND ccu.table_schema = tc.table_schema
      WHERE tc.constraint_type = 'FOREIGN KEY' AND tc.table_name='${tableName}'
    `, { type: Sequelize.QueryTypes.SELECT });

    return rows;
  }

  async getProps() {
    const tableNames = await this.getTableNames();

    const result = await Promise.all(tableNames.map(async (tableName) => {
      const columns = await this.getColumns(tableName);

      const foreignKeys = await this.getForeignKeys(tableName);

      return {
        tableName,
        columns: Object.keys(columns).reduce((previous, columnName) => {
          const foundForeignKey = foreignKeys.find((foreignKey) => foreignKey.columnName === columnName);

          const type = S(columns[columnName].type).startsWith(PostgresqlAdapter.TYPE_CHARACTER_VARYING) === true
            ? { name: PostgresqlAdapter.TYPE_CHARACTER_VARYING, length: columns[columnName].type.match(new RegExp(`^${PostgresqlAdapter.TYPE_CHARACTER_VARYING}\\(([0-9]+)\\)$`))[1] }
            : { name: columns[columnName].type }

          return [
            ...previous,
            {
              columnName,
              type: PostgresqlAdapter.TYPES_MAPPING[type.name](type.length),
              allowNull: columns[columnName].allowNull,
              defaultValue: columns[columnName].defaultValue,
              special: columns[columnName].special,
              primaryKey: columns[columnName].primaryKey,
              foreignKey: foundForeignKey || null,
            }
          ]
        }, []),
      };
    }));

    return result;
  }
}

module.exports = {
  PostgresqlAdapter,
};
