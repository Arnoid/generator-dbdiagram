# generator-dbdiagram [![NPM version][npm-image]][npm-url] [![Build Status][travis-image]][travis-url] [![Dependency Status][daviddm-image]][daviddm-url] [![Coverage percentage][coveralls-image]][coveralls-url]
> A generator to get a dbdiagram schema from a database

## Installation

First, install [Yeoman](http://yeoman.io) and generator-dbdiagram using [npm](https://www.npmjs.com/) (we assume you have pre-installed [node.js](https://nodejs.org/)).

```bash
npm install -g yo
cd generator-dbdiagram
npm link
```

Then generate your new project:

```bash
yo dbdiagram
```

## Getting To Know Yeoman

 * Yeoman has a heart of gold.
 * Yeoman is a person with feelings and opinions, but is very easy to work with.
 * Yeoman can be too opinionated at times but is easily convinced not to be.
 * Feel free to [learn more about Yeoman](http://yeoman.io/).

## License

MIT © [Arnaud Commelin](arnoid.dev)


[npm-image]: https://badge.fury.io/js/generator-dbdiagram.svg
[npm-url]: https://npmjs.org/package/generator-dbdiagram
[travis-image]: https://travis-ci.com/Arnoid31/generator-dbdiagram.svg?branch=master
[travis-url]: https://travis-ci.com/Arnoid31/generator-dbdiagram
[daviddm-image]: https://david-dm.org/Arnoid31/generator-dbdiagram.svg?theme=shields.io
[daviddm-url]: https://david-dm.org/Arnoid31/generator-dbdiagram
[coveralls-image]: https://coveralls.io/repos/Arnoid31/generator-dbdiagram/badge.svg
[coveralls-url]: https://coveralls.io/r/Arnoid31/generator-dbdiagram
